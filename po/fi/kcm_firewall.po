# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-firewall package.
# Tommi Nieminen <translator@legisign.org>, 2020, 2021, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-firewall\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-09 01:46+0000\n"
"PO-Revision-Date: 2023-05-28 16:57+0300\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.04.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Tommi Nieminen"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "translator@legisign.org"

#: backends/firewalld/firewalldclient.cpp:600 backends/ufw/ufwclient.cpp:697
#, kde-format
msgid "Error fetching information from the firewall."
msgstr "Virhe noudettaessa palomuurin tietoja."

#: backends/firewalld/firewalldjob.cpp:173
#, kde-format
msgid "firewalld saving (runtime to permanent)"
msgstr "firewalld tallentaa (ajonaikaisesta pysyväksi)"

#: backends/firewalld/firewalldjob.cpp:197
#, kde-format
msgid "firewalld saving"
msgstr "firewalld tallentaa"

#: backends/firewalld/firewalldjob.cpp:197
#, kde-format
msgid "firewalld %1"
msgstr "firewalld %1"

#: backends/firewalld/queryrulesfirewalldjob.cpp:50
#, kde-format
msgid "firewalld listing rules and services"
msgstr "firewalld luettelemassa sääntöjä ja palveluja"

#: backends/netstat/conectionsmodel.cpp:94
#, kde-format
msgid "Failed to get connections: %1"
msgstr "Yhteyksien noutaminen epäonnistui: %1"

#: backends/netstat/conectionsmodel.cpp:112
#, kde-format
msgid "Not Connected"
msgstr "Ei yhteyttä"

#: backends/netstat/conectionsmodel.cpp:114
#, kde-format
msgid "Established"
msgstr "Muodostettu"

#: backends/netstat/conectionsmodel.cpp:116
#, kde-format
msgid "Listening"
msgstr "Kuunnellaan"

#: backends/ufw/helper/helper.cpp:213
#, kde-format
msgid "Invalid arguments passed to the profile"
msgstr "Profiilille välitettiin virheellisiä parametreja"

#: backends/ufw/helper/helper.cpp:243
#, kde-format
msgid "Error saving the profile."
msgstr "Virhe tallennettaessa profiilia."

#: backends/ufw/helper/helper.cpp:267
#, kde-format
msgid "Invalid arguments passed to delete profile"
msgstr "Profiilin poistoon välitettiin virheellisiä parametreja"

#: backends/ufw/helper/helper.cpp:274
#, kde-format
msgid "Could not remove the profile from disk."
msgstr "Profiilia ei voitu poistaa levyltä."

#: backends/ufw/helper/helper.cpp:289
#, kde-format
msgid "Invalid argument passed to add Rules"
msgstr "Sääntöjen lisäykselle välitettiin virheellinen parametri"

#: backends/ufw/helper/helper.cpp:361
#, kde-format
msgid "An error occurred in command '%1': %2"
msgstr "Komennossa ”%1” tapahtui virhe: %2"

#: backends/ufw/ufwclient.cpp:161
#, kde-format
msgid ""
"There was an error in the backend! Please report it.\n"
"%1 %2"
msgstr ""
"Taustaosassa tapahtui virhe! Ilmoita siitä.\n"
"%1 %2"

#: backends/ufw/ufwclient.cpp:268
#, kde-format
msgid "Error fetching firewall logs: %1"
msgstr "Virhe noudettaessa palomuurin lokeja: %1"

#: backends/ufw/ufwclient.cpp:608 core/firewallclient.cpp:74
#, kde-format
msgid "Any"
msgstr "Mikä tahansa"

#: core/rule.cpp:46
#, kde-format
msgctxt "address on interface"
msgid "%1 on %2"
msgstr "%1 liitännässä %2"

#: core/rule.cpp:75 core/rule.cpp:165 core/rule.cpp:171
#, kde-format
msgid "Anywhere"
msgstr "Missä tahansa"

#: core/rule.cpp:96 core/rule.cpp:109 core/rule.cpp:126
#, kde-format
msgctxt "service/application name (port numbers)"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: core/rule.cpp:199
#, kde-format
msgctxt "firewallAction incoming"
msgid "%1 incoming"
msgstr "%1 saapuva"

#: core/rule.cpp:200
#, kde-format
msgctxt "firewallAction outgoing"
msgid "%1 outgoing"
msgstr "%1 lähtevä"

#: core/rule.cpp:205
#, kde-format
msgid "Yes"
msgstr "Kyllä"

#: core/systemdjob.cpp:51
#, kde-format
msgid "Invalid Call"
msgstr "Virheellinen kutsu"

#: core/types.cpp:19
#, kde-format
msgid "Off"
msgstr "Ei käytössä"

#: core/types.cpp:22
#, kde-format
msgid "Low"
msgstr "Vähäinen"

#: core/types.cpp:24
#, kde-format
msgid "Medium"
msgstr "Keskinkertainen"

#: core/types.cpp:26
#, kde-format
msgid "High"
msgstr "Suuri"

#: core/types.cpp:28
#, kde-format
msgid "Full"
msgstr "Täysi"

#: core/types.cpp:45 ui/RuleEdit.qml:38
#, kde-format
msgid "None"
msgstr "Ei mitään"

#: core/types.cpp:47
#, kde-format
msgid "New connections"
msgstr "Uudet yhteydet"

#: core/types.cpp:49
#, kde-format
msgid "All packets"
msgstr "Kaikki paketit"

#: core/types.cpp:58 ui/main.qml:25
#, kde-format
msgid "Allow"
msgstr "Salli"

#: core/types.cpp:60
#, kde-format
msgid "Deny"
msgstr "Estä"

#: core/types.cpp:62 ui/main.qml:27
#, kde-format
msgid "Reject"
msgstr "Hylkää"

#: core/types.cpp:64
#, kde-format
msgid "Limit"
msgstr "Rajoitus"

#: core/types.cpp:80
#, kde-format
msgid "Amule"
msgstr "Amule"

#: core/types.cpp:82
#, kde-format
msgid "Deluge"
msgstr "Deluge"

#: core/types.cpp:84
#, kde-format
msgid "KTorrent"
msgstr "KTorrent"

#: core/types.cpp:86
#, kde-format
msgid "Nicotine"
msgstr "Nicotine"

#: core/types.cpp:88
#, kde-format
msgid "qBittorrent"
msgstr "qBittorrent"

#: core/types.cpp:90
#, kde-format
msgid "Transmission"
msgstr "Transmission"

#: core/types.cpp:92
#, kde-format
msgid "ICQ"
msgstr "ICQ"

#: core/types.cpp:94
#, kde-format
msgid "Jabber"
msgstr "Jabber"

#: core/types.cpp:96
#, kde-format
msgid "Windows Live Messenger"
msgstr "Windows Live Messenger"

#: core/types.cpp:98
#, kde-format
msgid "Yahoo! Messenger"
msgstr "Yahoo! Messenger"

#: core/types.cpp:100
#, kde-format
msgid "FTP"
msgstr "FTP"

#: core/types.cpp:102
#, kde-format
msgid "HTTP"
msgstr "HTTP"

#: core/types.cpp:104
#, kde-format
msgid "Secure HTTP"
msgstr "Salattu HTTP"

#: core/types.cpp:106
#, kde-format
msgid "IMAP"
msgstr "IMAP"

#: core/types.cpp:108
#, kde-format
msgid "Secure IMAP"
msgstr "Salattu IMAP"

#: core/types.cpp:110
#, kde-format
msgid "POP3"
msgstr "POP3"

#: core/types.cpp:112
#, kde-format
msgid "Secure POP3"
msgstr "Salattu POP3"

#: core/types.cpp:114
#, kde-format
msgid "SMTP"
msgstr "SMTP"

#: core/types.cpp:116
#, kde-format
msgid "NFS"
msgstr "NFS"

#: core/types.cpp:118
#, kde-format
msgid "Samba"
msgstr "Samba"

#: core/types.cpp:120
#, kde-format
msgid "Secure Shell"
msgstr "SSH"

#: core/types.cpp:122
#, kde-format
msgid "VNC"
msgstr "VNC"

#: core/types.cpp:124
#, kde-format
msgid "Zeroconf"
msgstr "Zeroconf"

#: core/types.cpp:126
#, kde-format
msgid "Telnet"
msgstr "Telnet"

#: core/types.cpp:128
#, kde-format
msgid "NTP"
msgstr "NTP"

#: core/types.cpp:130
#, kde-format
msgid "CUPS"
msgstr "CUPS"

#: ui/About.qml:25
#, kde-format
msgid "About Firewall"
msgstr "Tietoa palomuurista"

#: ui/About.qml:32
#, kde-format
msgid "Backend:"
msgstr "Taustajärjestelmä:"

#: ui/About.qml:39
#, kde-format
msgid "Version:"
msgstr "Versio:"

#: ui/AdvancedRuleEdit.qml:31 ui/SimpleRuleEdit.qml:29
#, kde-format
msgid "Policy:"
msgstr "Käytänne:"

#: ui/AdvancedRuleEdit.qml:42 ui/SimpleRuleEdit.qml:40
#, kde-format
msgid "Direction:"
msgstr "Suunta:"

#: ui/AdvancedRuleEdit.qml:45 ui/SimpleRuleEdit.qml:44
#, kde-format
msgid "Incoming"
msgstr "Saapuva"

#: ui/AdvancedRuleEdit.qml:51 ui/SimpleRuleEdit.qml:50
#, kde-format
msgid "Outgoing"
msgstr "Lähtevä"

#: ui/AdvancedRuleEdit.qml:59
#, kde-format
msgid "IP Version:"
msgstr "IP:n versio:"

#: ui/AdvancedRuleEdit.qml:62
#, kde-format
msgid "IPv4"
msgstr "IPv4"

#: ui/AdvancedRuleEdit.qml:67
#, kde-format
msgid "IPv6"
msgstr "IPv6"

#: ui/AdvancedRuleEdit.qml:74
#, kde-format
msgid "Source:"
msgstr "Lähde:"

#: ui/AdvancedRuleEdit.qml:94
#, kde-format
msgid "Destination:"
msgstr "Kohde:"

#: ui/AdvancedRuleEdit.qml:114
#, kde-format
msgid "Protocol:"
msgstr "Yhteyskäytäntö:"

#: ui/AdvancedRuleEdit.qml:125
#, kde-format
msgid "Interface:"
msgstr "Käyttöliittymä:"

#: ui/AdvancedRuleEdit.qml:137
#, kde-format
msgid "Logging:"
msgstr "Loki:"

#: ui/ConnectionsView.qml:14
#, kde-format
msgid "Connections"
msgstr "Yhteydet"

#: ui/ConnectionsView.qml:18 ui/LogsView.qml:19
#, kde-format
msgid "Protocol"
msgstr "Yhteyskäytäntö"

#: ui/ConnectionsView.qml:19
#, kde-format
msgid "Local Address"
msgstr "Paikallinen osoite"

#: ui/ConnectionsView.qml:20
#, kde-format
msgid "Foreign Address"
msgstr "Vieras osoite"

#: ui/ConnectionsView.qml:21
#, kde-format
msgid "Status"
msgstr "Tila"

#: ui/ConnectionsView.qml:22
#, kde-format
msgid "PID"
msgstr "PID"

#: ui/ConnectionsView.qml:23
#, kde-format
msgid "Program"
msgstr "Ohjelma"

#: ui/ConnectionsView.qml:26
#, kde-format
msgid "There are currently no open connections"
msgstr "Ei avoimia yhteyksiä"

#: ui/ConnectionsView.qml:35
#, kde-format
msgid "Created a blacklist rule from this connection."
msgstr "Tästä yhteydestä luotiin torjuntasääntö."

#: ui/ConnectionsView.qml:52
#, kde-format
msgid "could not find iproute2 or net-tools packages installed."
msgstr "Asennettuja iproute2- tai net-tools-paketteja ei löytynyt."

#: ui/IpTextField.qml:14
#, kde-format
msgid "Any IP address"
msgstr "Mikä tahansa IP-osoite"

#: ui/LogsView.qml:16
#, kde-format
msgid "Firewall Logs"
msgstr "Palomuurin lokit"

#: ui/LogsView.qml:20 ui/main.qml:331
#, kde-format
msgid "From"
msgstr "Mistä"

#: ui/LogsView.qml:22 ui/main.qml:336
#, kde-format
msgid "To"
msgstr "Mihin"

#: ui/LogsView.qml:24
#, kde-format
msgid "Interface"
msgstr "Liitäntä"

#: ui/LogsView.qml:26
#, kde-format
msgid "There are currently no firewall log entries"
msgstr "Ei palomuurilokitietueita"

#: ui/LogsView.qml:37
#, kde-format
msgid "Created a blacklist rule from this log entry."
msgstr "Tästä lokitietueesta luotiin torjuntasääntö."

#: ui/main.qml:25
#, kde-format
msgid "Allow all connections"
msgstr "Salli kaikki yhteydet"

#: ui/main.qml:26
#, kde-format
msgid "Ignore"
msgstr "Ohita"

#: ui/main.qml:26
#, kde-format
msgid ""
"Keeps the program waiting until the connection attempt times out, some short "
"time later."
msgstr ""
"Odotuttaa ohjelmaa, kunnes yhteys jonkin ajan kuluttua aikakatkaistaan."

#: ui/main.qml:27
#, kde-format
msgid "Produces an immediate and very informative 'Connection refused' message"
msgstr "Tuottaa välittömän ja hyvin informatiivisen ”Yhteys torjuttu” -viestin"

#: ui/main.qml:44
#, kde-format
msgid "Create A New Firewall Rule"
msgstr "Luo uusi palomuurisääntö"

#: ui/main.qml:44
#, kde-format
msgid "Edit Firewall Rule"
msgstr "Muokkaa palomuurisääntöä"

#: ui/main.qml:67 ui/main.qml:246 ui/main.qml:381
#, kde-format
msgid "Please restart plasma firewall, the backend disconnected."
msgstr ""
"Käynnistä Plasman palomuuri uudelleen: taustajärjestelmä katkaisi yhteyden."

#: ui/main.qml:81 ui/ViewBase.qml:110
#, kde-format
msgid "Error creating rule: %1"
msgstr "Virhe luotaessa sääntöä: %1"

#: ui/main.qml:83
#, kde-format
msgid "Error updating rule: %1"
msgstr "Virhe päivitettäessä sääntöä: %1"

#: ui/main.qml:108
#, kde-format
msgid "Create"
msgstr "Luo"

#: ui/main.qml:108
#, kde-format
msgid "Save"
msgstr "Tallenna"

#: ui/main.qml:132
#, kde-format
msgid "Firewall Status:"
msgstr "Palomuurin tila:"

#: ui/main.qml:138
#, kde-format
msgid "Disabling…"
msgstr "Poistetaan käytöstä…"

#: ui/main.qml:138
#, kde-format
msgid "Enabled"
msgstr "Käytössä"

#: ui/main.qml:140
#, kde-format
msgid "Enabling…"
msgstr "Otetaan käyttöön…"

#: ui/main.qml:140
#, kde-format
msgid "Disabled"
msgstr "Ei käytössä"

#: ui/main.qml:166
#, kde-format
msgid "The firewall application, please install %1"
msgstr "Palomuurisovellus: asenna %1"

#: ui/main.qml:180
#, kde-format
msgid "Permission denied"
msgstr "Ei käyttöoikeutta"

#: ui/main.qml:184
#, kde-format
msgid ""
"You recently updated your kernel. Iptables is failing to initialize, please "
"reboot."
msgstr ""
"Ydin on vastikään päivittynyt. iptablesin alustaminen ei onnistu: käynnistä "
"kone uudelleen."

#: ui/main.qml:187
#, kde-format
msgid "Error enabling firewall: %1"
msgstr "Virhe otettaessa palomuuria käyttöön: %1"

#: ui/main.qml:188
#, kde-format
msgid "Error disabling firewall: %1"
msgstr "Virhe poistettaessa palomuuria käytöstä: %1"

#: ui/main.qml:208
#, kde-format
msgid "Default Incoming Policy:"
msgstr "Saapuva oletuskäytänne:"

#: ui/main.qml:209
#, kde-format
msgid "Default Outgoing Policy:"
msgstr "Lähtevä oletuskäytänne:"

#: ui/main.qml:256
#, kde-format
msgid "Error changing policy: %1"
msgstr "Virhe muutettaessa käytännettä: %1"

#: ui/main.qml:319
#, kde-format
msgid "Firewall is disabled"
msgstr "Palomuuri ei ole käytössä"

#: ui/main.qml:319
#, kde-format
msgid "No firewall rules have been added"
msgstr "Palomuurisääntöjä ei ole lisätty"

#: ui/main.qml:321
#, kde-kuit-format
msgctxt "@info"
msgid "Click the <interface>Add Rule…</interface> button below to add one"
msgstr "Lisää sääntö napsauttamalla alta <interface>Lisää sääntö…</interface>"

#: ui/main.qml:322
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Enable the firewall with the <interface>Firewall Status</interface> checkbox "
"above, and then click the <interface>Add Rule…</interface> button below to "
"add one"
msgstr ""
"Ota palomuuri käyttöön yltä <interface>Palomuurin tila</interface> -"
"valintaruudusta ja lisää sääntöjä napsauttamalla alta <interface>Lisää "
"sääntö…</interface>"

#: ui/main.qml:326
#, kde-format
msgid "Action"
msgstr "Toiminto"

#: ui/main.qml:341
#, kde-format
msgid "IP"
msgstr "IP"

#: ui/main.qml:346
#, kde-format
msgid "Logging"
msgstr "Loki"

#: ui/main.qml:372
#, kde-format
msgctxt "@info:tooltip"
msgid "Edit Rule"
msgstr "Muokkaa sääntöä"

#: ui/main.qml:392
#, kde-format
msgid "Error removing rule: %1"
msgstr "Virhe poistettaessa sääntöä: %1"

#: ui/main.qml:399
#, kde-format
msgctxt "@info:tooltip"
msgid "Remove Rule"
msgstr "Poista sääntö"

#: ui/main.qml:409
#, kde-format
msgctxt "'view' is being used as a verb here"
msgid "View Connections"
msgstr "Näytä yhteydet"

#: ui/main.qml:414
#, kde-format
msgctxt "'view' is being used as a verb here"
msgid "View Logs"
msgstr "Näytä loki"

#: ui/main.qml:425
#, kde-format
msgid "Add Rule…"
msgstr "Lisää sääntö…"

#: ui/main.qml:434
#, kde-format
msgid "About"
msgstr "Tietoa"

#: ui/main.qml:442
#, kde-format
msgid "Please install a firewall, such as ufw or firewalld"
msgstr "Asenna palomuuri kuten ufw tai firewalld"

#: ui/PortTextField.qml:11
#, kde-format
msgid "Any Port"
msgstr "Mikä tahansa portti"

#: ui/RuleEdit.qml:39
#, kde-format
msgid "New Connections"
msgstr "Uudet yhteydet"

#: ui/RuleEdit.qml:40
#, kde-format
msgid "All Packets"
msgstr "Kaikki paketit"

#: ui/RuleEdit.qml:55 ui/RuleEdit.qml:62
#, kde-format
msgid "The default incoming policy is already '%1'."
msgstr "Saapuvan liikenteen oletuskäytänne on jo ”%1”."

#: ui/RuleEdit.qml:56 ui/RuleEdit.qml:63
#, kde-format
msgid "The default outgoing policy is already '%1'."
msgstr "Lähtevän liikenteen oletuskäytänne on jo ”%1”."

#: ui/SimpleRuleEdit.qml:23
#, kde-format
msgid "Allow connections for:"
msgstr "Salli seuraavat yhteydet:"

#: ui/SimpleRuleEdit.qml:23
#, kde-format
msgid "Application:"
msgstr "Sovellus:"

#: ui/ViewBase.qml:224
#, kde-format
msgid "Blacklist Connection"
msgstr "Torju yhteys"

#~ msgctxt "@title"
#~ msgid "Configure Firewall"
#~ msgstr "Palomuurin asetukset"

#~ msgctxt "@info:credit"
#~ msgid "Alexis López Zubieta"
#~ msgstr "Alexis López Zubieta"

#~ msgctxt "@info:credit"
#~ msgid "Tomaz Canabrava"
#~ msgstr "Tomaz Canabrava"

#~ msgctxt "@info:credit"
#~ msgid "Lucas Januario"
#~ msgstr "Lucas Januario"

#~ msgid "Simple Rule Edit:"
#~ msgstr "Yksinkertainen säännön muokkaus:"

#~ msgid "Connections..."
#~ msgstr "Yhteydet…"

#~ msgid "Logs..."
#~ msgstr "Lokit…"

#~ msgid ""
#~ "Could not open the log file for the ufw client. \n"
#~ " Please verify that the following file exist \n"
#~ " and you have read permissions. \n"
#~ msgstr ""
#~ "UFW-asiakkaan lokitiedostoa ei voitu avata.\n"
#~ "Varmista, että seuraava tiedosto on olemassa\n"
#~ "ja että siihen on lukuoikeus.\n"

#~ msgid "TCP"
#~ msgstr "TCP"

#~ msgid "UDP"
#~ msgstr "UDP"

#~ msgid "Any protocol"
#~ msgstr "Mikä tahansa yhteyskäytäntö"

#~ msgid "This module lets you configure firewall."
#~ msgstr "Tässä osiossa voi asettaa palomuurin."

#~ msgid "Ip"
#~ msgstr "IP"
